# Quickstart

Для работы плагина необходимо объявить глобальные константы в wp-config.php

```sh
define('INSTAGRAM_TOKEN', INSTAGRAM_TOKEN);
define('INSTAGRAM_APP_ID', INSTAGRAM_APP_ID);
```

Инструкция для получения токена:
При получении токена необходимо указать следующие разрешения: read_insights, read_audience_network_insights, manage_pages, pages_show_list, instagram_basic, instagram_manage_comments, instagram_manage_insights, public_profile
```
https://stackoverflow.com/questions/17197970/facebook-permanent-page-access-token/28418469#28418469
```


Инструкция для получения app id:

```
https://developers.facebook.com/docs/instagram-api/getting-started#test
```
INSTAGRAM_APP_ID - это id полученный при запросе instagram_business_account