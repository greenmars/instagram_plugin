<?php
/**
 * @package Instagram_greenmars
 */
/*
Plugin Name: Instagram Plugin by Greenmars
Plugin URI: https://greenmars.ru
Description: Get data from user's Instagram via Graph Api Facebook.
Version: 1.0
Author: Greenmars
Author URI: https://greenmars.ru
License: GPLv2 or later
Text Domain: greenmars
*/
if ( ! defined( 'WPINC' ) ) {
	die;
}

class GM_InstagramPlugin {

	function __construct() {

		register_activation_hook( __FILE__, array($this, 'activate_instagram') );

		register_deactivation_hook( __FILE__, array($this, 'deactivate_instagram') );

		if ( !defined('INSTAGRAM_TOKEN') || !defined('INSTAGRAM_APP_ID') || !defined('INSTAGRAM_COUNT') ) {

			add_action( 'admin_notices', array($this, 'sample_admin_notice__error') );
		}
	}

	public function sample_admin_notice__error() {?>
	    <?php if ( !defined('INSTAGRAM_TOKEN') || !defined('INSTAGRAM_APP_ID') ) :?>
		    <div class="notice notice-error">
		        <p><?php _e( "Constants INSTAGRAM_TOKEN and INSTAGRAM_APP_ID aren't defined", 'instagram_greenmars' ); ?></p>
		    </div>
		<?php endif;?>
		<?php if ( !defined('INSTAGRAM_COUNT') ):?>
		    <div class="notice notice-warning">
		        <p><?php _e( "Constant INSTAGRAM_COUNT isn't defined", 'instagram_greenmars' ); ?></p>
		    </div>
		<?php endif;
	}

	public function activate_instagram() {
		if ( !defined('INSTAGRAM_TOKEN') || !defined('INSTAGRAM_APP_ID') ){
			return;
		}

		load_plugin_textdomain( 'instagram_greenmars', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		self::getPosts();
		add_action( 'gm_instagram_hook_posts', array('GM_InstagramPlugin', 'getPosts') );
		wp_schedule_event( time(), 'twicedaily', 'gm_instagram_hook_posts', array());
		
		self::getAccountData();
		add_action( 'gm_instagram_hook_media', array('GM_InstagramPlugin', 'getAccountData') );
		wp_schedule_event( time(), 'twicedaily', 'gm_instagram_hook_media', array());
	}

	public function deactivate_instagram() {
		wp_clear_scheduled_hook( 'gm_instagram_hook_posts', array() );
		wp_clear_scheduled_hook( 'gm_instagram_hook_media', array() );
		delete_transient( 'instagram_posts' );
		delete_transient( 'instagram_media' );
	}

	public static function getPosts() {

		if ( !defined('INSTAGRAM_TOKEN') || !defined('INSTAGRAM_APP_ID') )
			return;
		
		$posts = get_transient( 'instagram_posts' );
		if( !$posts ) {
			$ch = curl_init();

		    $url = 'https://graph.facebook.com/v3.0/'.INSTAGRAM_APP_ID.'/media?access_token='.INSTAGRAM_TOKEN;
		    if(defined('INSTAGRAM_COUNT'))
		    	$url .= '&limit='.INSTAGRAM_COUNT;

		    $curl_base_options = [
	            CURLOPT_URL => $url,
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_HEADER => true,
	            CURLOPT_ENCODING => ''
	        ];

	        curl_setopt_array($ch, $curl_base_options);
	        $response   = curl_exec($ch);
	        $error      = curl_error($ch);
	        $info       = curl_getinfo($ch);

	        $header_size = $info['header_size'];
	        $header      = substr($response, 0, $header_size);
	        $body        = substr($response, $header_size);
	        $httpCode    = $info['http_code'];

	        if($httpCode != '200') {

	        	if(is_admin())
	        		echo 'HTTP код: '.$httpCode;
	        	return;
	        }

	        $media = json_decode($body, true);
	        
	        $media = $media['data'];
	        
	        $item = array();
	        if(!empty($media)) {

	        	foreach($media as $key => $image) {
		        	$url = 'https://graph.facebook.com/v3.0/'.$image['id'].'?fields=id,media_type,media_url,owner,timestamp,permalink,thumbnail_url,caption,like_count&access_token='.INSTAGRAM_TOKEN;

		        	$curl_base_options = [
			            CURLOPT_URL => $url,
			            CURLOPT_RETURNTRANSFER => true,
			            CURLOPT_HEADER => true,
			            CURLOPT_ENCODING => ''
			        ];

			        curl_setopt_array($ch, $curl_base_options);
			        $response   = curl_exec($ch);
			        $error      = curl_error($ch);
			        $info       = curl_getinfo($ch);

			        $header_size = $info['header_size'];
			        $header      = substr($response, 0, $header_size);
			        $body        = substr($response, $header_size);
			        $httpCode    = $info['http_code'];

			        if($httpCode == '200') {
			        	$answer = json_decode($body, true);
			        	if($answer['media_type'] == 'VIDEO') {
			        		$item['src'] = $answer['thumbnail_url'];
			        	} else
			        		$item['src'] = $answer['media_url'];

			        	$item['link'] = $answer['permalink'];
			        	$item['alt'] = $answer['caption'];
			        	$item['likes'] = $answer['like_count'];
			        	$item['timestamp'] = $answer['timestamp'];
			        	$posts['ITEMS'][] = $item;
			        }
		        }
	        }

			curl_close($ch);
			if ( !empty($posts) )
				set_transient( 'instagram_posts', $posts, DAY_IN_SECONDS / 2);
		}

		return $posts;
	}

	public static function getAccountData() {

		if ( !defined('INSTAGRAM_TOKEN') || !defined('INSTAGRAM_APP_ID') )
			return;

		$media = get_transient( 'instagram_media' );
		if( !$media ) {

			$ch = curl_init();
			$url = 'https://graph.facebook.com/v3.0/'.INSTAGRAM_APP_ID.'?fields=id,username,followers_count,follows_count,media_count,name,profile_picture_url&access_token='.INSTAGRAM_TOKEN;
			$curl_base_options = [
	            CURLOPT_URL => $url,
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_HEADER => true,
	            CURLOPT_ENCODING => ''
	        ];
			curl_setopt_array($ch, $curl_base_options);
	        $response   = curl_exec($ch);
	        $error      = curl_error($ch);
	        $info       = curl_getinfo($ch);

	        $header_size = $info['header_size'];
	        $header      = substr($response, 0, $header_size);
	        $body        = substr($response, $header_size);
	        $httpCode    = $info['http_code'];
	        $media = array();
	        if($httpCode == '200') {
				$user = json_decode($body, true);
				$media['COUNTS']['MEDIA'] = $user['media_count'];
		  		$media['COUNTS']['FOLLOWS'] = $user['follows_count'];
		  		$media['COUNTS']['FOLLOWED_BY'] = $user['followers_count'];
		  		$media['NAME'] = $user['name'];
		  		$media['USERNAME'] = $user['username'];
		  		$media['LINK'] = 'https://www.instagram.com/'.$user['username'].'/';
		  		$media['PROFILE_PICTURE'] = $user['profile_picture_url'];
		  	}
		  	curl_close($ch);

		}
		
		if ( !empty($media) )
			set_transient( 'instagram_media', $media, DAY_IN_SECONDS / 2);
		return $media;
	}
}

new GM_InstagramPlugin();